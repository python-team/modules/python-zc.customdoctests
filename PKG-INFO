Metadata-Version: 1.1
Name: zc.customdoctests
Version: 1.0.1
Summary: =====================================================
Home-page: http://pypi.python.org/pypi/zc.customdoctests
Author: Jim Fulton
Author-email: jim@zope.com
License: ZPL 2.1
Description: =====================================================
        zc.customdoctests -- Use doctest with other languages
        =====================================================
        
        doctest (and recently manuel) provide hooks for using custom doctest
        parsers.  `zc.customdoctests` helps to leverage this to support other
        languages, such as JavaScript::
        
            js> function double (x) {
            ...     return x*2;
            ... }
            js> double(2)
            4
        
        And with `manuel <http://pypi.python.org/pypi/manuel>`_, it
        facilitates doctests that mix multiple languages, such as Python,
        JavaScript, and sh.
        
        .. contents::
        
        
        Detailed documentation
        ======================
        
        Custom doctest parsers
        ----------------------
        
        zc.customdoctests provides a little bit of help with creating custom
        doctest parsers that work pretty muct like regular doctests, but that
        use an alternate means of evaluating examples.  To use it, you call
        zc.customdoctests.DocTestParser and pass any of the following options:
        
        ps1
           The first-line prompt, which defaultd to ``'>>>'``.
        
           This must be a regular expression that matches exactly 3 characters.
        
           (Note that you can't override the second-line prompt.)
        
        comment_prefix
           The comment prefix regular expression, which defaults to '#'.
        
        transform
           A function used to transform example source, which defaults to a
           no-operation function.
        
        The js module provides support for using JavaScript in doctests using
        `python-spidermonkey
        <http://pypi.python.org/pypi/python-spidermonkey>`_. It provides some
        examples of defining custom doctest parsers.
        
        
        Javascript and Python-Spidermonkey support
        ------------------------------------------
        
        .. This file shows some examples of using spidermonkey APIs in doctests.
        
        To wire this up, you'd use something like::
        
           import doctest, zc.customdoctests.js
        
           test_suite = doctest.DocTestSuite(
               parser=zc.customdoctests.js.parser,
               setUp=zc.customdoctests.js.spidermonkeySetUp)
        
        Or, with manuel::
        
            test_suite = manuel.testing.TestSuite(
                manuel.doctest.Manuel(parser=zc.customdoctests.js.parser) +
                manuel.doctest.Manuel(parser=zc.customdoctests.js.eq_parser) +
                manuel.doctest.Manuel() +
                manuel.capture.Manuel(),
                'spidermonkey.txt',
                setUp=zc.customdoctests.js.spidermonkeySetUp)
        
        Note that zc.customdoctests doesn't require spidermonkey, so you need
        to install spidermonkey seperately if you want to use it.
        
        An advantage of using manuel is that you can use multiple parsers in
        the same document.  In the example, above, 2 javascript example
        syntaxes (described below) as well as the standard doctest syntax are
        supported.  This document is run with manuel to allow all 3 syntaxes.
        
        For the rest of this document, we'll show examples of JavaScript
        doctests as well as helper APIs used to support JavaScript and to
        integrate JavaScript and Python.
        
        Javascript doctests use a "js>" prompt (as used in rhino and the
        spidermonkey interpreter)::
        
            js> 2 +
            ... 'Hi world' // doctest: +ELLIPSIS
            u'2Hi...
        
        Assignments return values.  This can generate annoying output
        in doctests::
        
            js> ob = {a: 1, b: 2}
            [object Object]
        
        If you're using manuel, you can avoid this by using js!::
        
            js! x = 3
        
        which suppresses expression values.
        
        load and print functions (similar to those found in rhino) are
        provided.  For example, given a javascript file, double.js::
        
           function double (x) {
               return x*2;
           }
        
        .. -> src
        
           >>> with open('double.js', 'w') as f:
           ...     f.write(src)
        
        We can load the file::
        
            js> load('double.js')
            js> double(10)
            20
        
        We can print values::
        
            js> print('Hi')
            Hi
        
        A python object provides access to the open function and the os
        module::
        
            js> python.os.path.exists('double.js')
            True
        
            js! f = python.open('double.js')
            js> print(f.read())
            function double (x) {
                return x*2;
            }
            <BLANKLINE>
        
            js> f.close()
        
        
        If you're using manuel, you can intermix Python and and JavaScript
        examples and there are a number of APIs to facilitate using Python and
        JavaScript together.
        
        There's an add_js_global function to copy data from Python::
        
            >>> add_js_global('y', 1)
        
            js> y
            1
        
        There's also a js object that provides attribute access to js globals::
        
            >>> js.x
            3
        
            >>> js.z = 4
        
            js> z
            4
        
        You can also call this to run JS code without returning the resulting value::
        
            >>> js('a = x + y')
        
            js> a
            4
        
        
        Changelog
        =========
        
        1.0.1 (2013-02-14)
        ------------------
        
        - Nothing changed yet.
        
        
        1.0.0 (2013-02-13)
        ------------------
        
        - Added Python 3.3 support.
        
        - Cleanup `setup.py`, add `tox.ini` and manifest.
        
        
        0.1.0 (2011-05-19)
        ------------------
        
        - Initial release
        
Platform: UNKNOWN
Classifier: Development Status :: 4 - Beta
Classifier: Intended Audience :: Developers
Classifier: License :: OSI Approved :: Zope Public License
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 2.6
Classifier: Programming Language :: Python :: 2.7
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.3
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Natural Language :: English
Classifier: Operating System :: OS Independent
Classifier: Topic :: Software Development
